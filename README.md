# Filtros uBlock Origin PT-PT

Lista mantida pelo coletivo __chapeubranco__.

Lista de regras para [uBlock Origin](https://github.com/gorhill/uBlock) destinada a websites portugueses de Portugal.

Para websites do Brasil, por favor reportar ao projeto [easylistportuguese](https://github.com/easylist/easylistportuguese)
e/ou [EasyList Brasil](https://github.com/easylistbrasil/easylistbrasil).

Recomendado para navegadores web baseados em **Mozilla Firefox**, devido a proteção extra contra [CNAME cloacking entre outros](https://github.com/gorhill/uBlock/wiki/uBlock-Origin-works-best-on-Firefox).

uBlock Origin está disponível também para navegadores web, baseados em Mozilla Firefox, em sistemas Android [aqui](https://addons.mozilla.org/addon/ublock-origin/).

Para usar as novas regras no uBlock Origin, adiciona manualmente o seguinte endereço na lista de filtros (ver imagem):

```
https://codeberg.org/chapeubranco/filtros/raw/branch/master/filtros/filtros.txt
```

![Adicionar lista](assets/adicionar_lista.png) 

## Perguntas mais frequentes

### Queria contribuir para o projeto, mas só tenho conta no github. Que alternativas existem para poder contribuir?

Ler o artigo da Software Freedom Conservancy sobre [Give Up GitHub!](https://sfconservancy.org/GiveUpGitHub/) do porquê de este projeto estar hospedado no CodeBerg.

[![Não fazer upload para o github](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

O git é um protocolo decentralizado, podes contribuir [atravês do envio de patchs por email](https://git-send-email.io/).


## Contribuição

Pull requests são bem vindos. Ler [como criar os teus próprios filtros](https://kb.adguard.com/en/general/how-to-create-your-own-ad-filters), 
[uBlock Static filter syntax](https://github.com/gorhill/uBlock/wiki/Static-filter-syntax) ou consultar [esta cheatsheet](https://github.com/DandelionSprout/adfilt/blob/master/Wiki/SyntaxMeaningsThatAreActuallyHumanReadable.md).


## Agradecimentos

Este projeto não seria possível sem o apoio do [Codeberg](https://codeberg.org/) e do coletivo [Riseup](https://riseup.net/).

## Licença

Este trabalho está licenciado com uma Licença **Creative Commons - Atribuição 4.0 Internacional (CC BY 4.0)**.
