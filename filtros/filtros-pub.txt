! Publicidade
!
! Filtros cosméticos Gerais
##.ad-banner
##.ad-mobile
##.ad-standard
##.ad-sidebar-mrec
##.ad-wrapper
##.ads-by-google
##.adSlot
##.AdSense
##.afs_ads
##.block-ads
##.bloco-publicidade
##.bottom-ad
##.c-publicity
##.c-publicity-article-bottom
##.c-publicity-inside-article
##.c-publicity-sidebar
###div-ad-top
##.pubad_mobile
##.publicidad
##.publicidade
##.pubContainer
##.pubWrapper
##.pubheader
##.pub-box
##.pub_container
##.pub_mrec
##.pub_txt
##.pub-container
##.pub-space
##.patrocinadosWrapper
##.m-rec
##.m-rec_pub
##.mrecbox
##.mrec-container
##.mrec-dark
##.mrec-pub-box
##.mrec-wrapper
##.mrec-status
##.mrec-home
##.mrec_desktop
##.native_ad
##.t-pub-box-1
##.t-pubbox-mrec-1
##.td-a-ad
##.td-a-rec
##.td-a-rec-id-custom_ad_1
##.td-a-rec-id-custom_ad_2
##.td-a-rec-id-custom_ad_3
##.td-a-rec-id-custom_ad_4
##.td-adspot-title
##.td-scroll-up
###td-ad-placeholder
##.vce-header-ads
##.vce-ad-container
##.video-ad
##.ad_box
##.adrotate_widgets
##.adtester-container
##.adthrive
##.adthrive-ad
##.adthrive-content
##.ad-container
##.ad-container-inner
##.ad--desktop
##.ad--mobile
##.adwrap-mrec
##.adcontainer
##.bannerBox
##.ezoic-ad
##.ezoic-adpicker-ad
##.ezoic-ad-adaptive
##.ez-sidebar-wall-ad
##.ezo_ad
###ez-sidebar-wall-left
###hpAdVideo
##.info_pub
##.responsive-ad
##.leaderboard-ad
##.widget-pub
###adContainer
###ad_bottom
###ad_top
###adsbox
###adsense_top
###adb-enabled
###ads-blog
###ads-content
###bsu-placeholder
##.adsbygoogle
##.adWrapper
##.adv_txt
##.advert-wrap
##.advert-image
##.widget-ad
##.widget-ad-image
##.widget-ad-script
##.ad-description
##.ad-header
##.ad-mrec
##.ad-slot__label
##.ad-widget-box
##.ad-widget-sizes
##.ad-wrap
##.ad-zone
##.adk-slot
##.ads-home
##.ads-label
##.ads-top
##.ads-sidebar
##.ads-bottom
##.ads-wrapper
##.article-inline-ad
##.article-taboola
##.header-ad
##.header-ads
##.header-ad-container
##.tracking-consent
##.header-area-pub
##.probably-ads
##.middle-ad
##.mobile-ad
##.sponsor-inner
##.sponsor-link
##.sub-top-ad
##.top-ad
##.footer-adv
##.gallery-ad
##.js-ad
##.jeg_ad
##.jeg_ad_top
##.jnews_header_top_ads
##iframe[id^="google_ads_frame"]

! Filtros cosméticos particulares
! https://www.publico.pt
publico.pt##.stack__ads
publico.pt##.paywall__content
publico.pt##.site-message--paywall
publico.pt##.datawall-warning
publico.pt##.stack-social-tools
publico.pt##.aside--ads
publico.pt##.follow-us
publico.pt##.ad-slot
publico.pt##.ad-slot--double
publico.pt##.ad-slot--sticky
publico.pt##.ad-slot__wrapper
publico.pt##.pubtxt
publico.pt###pub_4
publico.pt###disclaimer-inline
publico.pt,jornaldenegocios.pt##.egoi
publico.pt###banner-and-header > .pubHorzs

! https://www.noticiasaominuto.com
noticiasaominuto.com##.escolha-consumidor-badge-content
noticiasaominuto.com##.ultima-hora > a[href^="https://www.flytap.com"]
noticiasaominuto.com##.pub-container-cofidis
noticiasaominuto.com##.pub-container-block

! https://www.jn.pt
jn.pt##.js-article-footer.t-article-footer
jn.pt##.t-pub-box-1

! https://www.dn.pt
dn.pt##.t-pubbox-ct-1

! https://www.dinheirovivo.pt
dinheirovivo.pt,tsf.pt##.t-pubbox-mrec-1
dinheirovivo.pt,tsf.pt##.t-abc-footer-share
dinheirovivo.pt,tsf.pt##.t-f-bar-apps
dinheirovivo.pt,tsf.pt##.t-section-sponsored

! https://www.jornaldenegocios.pt
jornaldenegocios.pt,record.pt##.m-rec
record.pt##.masthead_BG

! https://sapo.pt
!sapo.pt##.loading.article-comments
sapo.pt##.ad
sapo.pt##.fugly-ads
sapo.pt##.pub
sapo.pt##.sapoPub
sapo.pt##.special-ctn
sapo.pt##.tin-pub-inner
sapo.pt##.top-pub
sapo.pt##div.pub-header
sapo.pt###features-promotion

! https://eco.sapo.pt
eco.sapo.pt##.infobox--post-article

! https://www.naturitas.pt
naturitas.pt##.register-popup-coupon

! https://freguesias.dnoticias.pt
freguesias.dnoticias.pt##.section-post-intro-share

! https://expresso.pt
expresso.pt##.ad-in-body
expresso.pt##.ad-in-sidebar
expresso.pt###paywall-exclusive-blocker

! https://volantesic.pt
volantesic.pt##.vsFooterSMedia

! https://poligrafo.sapo.pt
poligrafo.sapo.pt##[src="/assets/img/poligrafo/ftpfcp-02.png"]

! https://www.worten.pt
worten.pt##.appDownload
worten.pt##.widget

! https://www.e-konomista.pt
e-konomista.pt##.r-top-publicity-wrapper

! https://www.pcmanias.com
pcmanias.com##div.widget_text.sb-widget

! https://www.diarioimobiliario.pt
diarioimobiliario.pt##.singlePub
diarioimobiliario.pt##.topBanner

! https://caras.sapo.pt
caras.sapo.pt##.sticky-footer

! https://visao.sapo.pt
visao.sapo.pt##.tin-pub-inner
visao.sapo.pt##.sticky-footer

! https://www.portugal.gov.pt
portugal.gov.pt##ul:nth-of-type(4)

! https://www.fidelidade.pt
!fidelidade.pt##.footer_dark_social

! https://multinews.sapo.pt
multinews.sapo.pt##.adcontainer
multinews.sapo.pt##.header-left-menu
multinews.sapo.pt###yxzmwgdi-433-1261705218

! https://comunidadeculturaearte.com
comunidadeculturaearte.com##.pub-label
comunidadeculturaearte.com##.pt-1

! https://viva-porto.pt
viva-porto.pt##.le_ads
viva-porto.pt##.le_noticia_ads

! https://blogs.sapo.pt
blogs.sapo.pt##.pub_span

! https://observador.pt
observador.pt##.server-free-user
observador.pt##.obs-ad-placeholder
observador.pt##.obs-ad-placeholder-manchete
observador.pt##.obs-ad-placeholder-manchete-mobile

! https://www.brigantia.pt
brigantia.pt##.view-publicidade-site

! https://www.vozdapovoa.com
vozdapovoa.com##.img_publicidade

! https://www.jornalnordeste.com
jornalnordeste.com##.view-publicidade-site

! https://radioaltominho.pt
radioaltominho.pt##.single-new-pub

! https://www.zerozero.pt
zerozero.pt##.pubframe

! https://www.diariodetrasosmontes.com
diariodetrasosmontes.com##.view-publicidade

! https://www.iol.pt
iol.pt##.pub
iol.pt,meo.pt##.iol-comercial-placement
tviplayer.iol.pt##.billboardxlWrapper

! https://supercasa.pt
supercasa.pt##.partnership-advertising-banner

! https://vidaimobiliaria.com
vidaimobiliaria.com##.home-square-banner

! https://www.jm-madeira.pt
jm-madeira.pt##.anuncio_top

! https://www.diariodominho.pt
diariodominho.pt##.hidden-xs.pub_data

! https://www.diariocoimbra.pt
diariocoimbra.pt###pubsec2

! https://regiao-sul.pt
regiao-sul.pt###lt_banner_gallery_widget-3
regiao-sul.pt###lt_banner_widget-3
regiao-sul.pt###lt_banner_widget-4
regiao-sul.pt###lt_banner_widget-5
regiao-sul.pt###lt_banner_widget-6
regiao-sul.pt###lt_banner_widget-7
regiao-sul.pt###lt_banner_widget-8
regiao-sul.pt###lt_banner_widget-10
regiao-sul.pt###lt_banner_widget-11
regiao-sul.pt###lt_banner_widget-13
regiao-sul.pt###lt_banner_widget-14
regiao-sul.pt##.regia-adlabel

! https://www.dicasonline.com
dicasonline.com##.code-block-1
dicasonline.com##.code-block-2
dicasonline.com##.code-block-3

! https://www.eurogamer.pt
eurogamer.pt##.advert
eurogamer.pt##.adverts

! https://www.campeaoprovincias.pt
campeaoprovincias.pt##.pub-image

! https://www.forbespt.com
forbespt.com##.pub--landscape

! https://forum.pt
forum.pt##.bannergroup

! https://www.briefing.pt
briefing.pt##.pub-label
briefing.pt##.pub-header

! https://justnews.pt
justnews.pt##.fullscreen-ad
justnews.pt##div.sponsor_widget

! https://www.algarveprimeiro.com
algarveprimeiro.com##.smartpub

! https://www.standvirtual.com
standvirtual.com##[data-testid="featured-dealer"]
standvirtual.com##[data-testid="horizontal-top-ads"]
standvirtual.com##article[data-id]:has-text(Destacado)

! https://gamemarket.pt
gamemarket.pt###ad_banners_top-bottom

! https://averdade.com
averdade.com##.averd-target

! https://www.pcguia.pt
! https://www.imediato.pt
pcguia.pt,imediato.pt##.adv-link

! https://www.sulinformacao.pt
sulinformacao.pt##.ad

! https://www.sabado.pt
sabado.pt##.boost_activate
sabado.pt###widget_comercial

! https://www.diariodigitalcastelobranco.pt
diariodigitalcastelobranco.pt##.pub_cat_right

! https://www.guimaraesdigital.pt
guimaraesdigital.pt##.bannergroup

! https://jornaldofundao.pt
jornaldofundao.pt##span.banner-info-text

! https://ominho.pt
ominho.pt###wpgtr_stickyads_textcss_container

! https://rfm.pt
rfm.pt##.pub-info-hp
rfm.pt###top-pub
rfm.pt##aside

! Filtros não cosméticos
||ad.22betpartners.com^
||cdn.jwplayer.com/v2/advertising/*$xhr

/publicidade/*
/publicidade_
/display-ads.
/stickyads.
/images/ad/*
! wordpress plugins
/plugins/meks-easy-ads-widget/*
/plugins/ads-for-wp/
/plugins/google-listings-and-ads/
/plugins/advanced-ads-layer/
/plugins/sticky-ad-lightweight/

! https://www.jm-madeira.pt
||jm-madeira.pt/file/index/anuncios/*$image
||jm-madeira.pt/sondagens/get_sondagem_data/*$xhr

! https://www.diarioaveiro.pt
||diarioaveiro.pt/api/assets/download/publicities/*$image

! https://www.diariocoimbra.pt
||diariocoimbra.pt/api/assets/download/publicities/*$image

! https://www.vidaeconomica.pt
||vidaeconomica.pt/sites/all/files/*/banner*$image

! https://motorguia.net
||ads.motorguia.pt^

! https://www.revistasauda.pt
||revistasauda.pt/SiteCollectionImages/Banners/$image

! https://www.saberviver.pt
||saberviver.pt/wp-json/ads/

! https://justnews.pt
||justnews.pt/documentos/banners/*$image

! https://www.algarveprimeiro.com
||algarveprimeiro.com/pubs.txt$xhr

! https://tugatech.com.pt
||adbox.tugatech.com.pt^

! https://www.zerozero.pt
||zerozero.pt/script/zzsc.js

! https://www.jb.pt
||publicidade.novcomunicacao.pt^

! https://supercasa.pt
||supercasa.pt/images/partnershipadvertising/$image

! https://www.radiovizela.pt
||radiovizela.pt/gestao/img_publicidade/*$image